
# Synopsis #
___

Tengo la necesidad de proporcionar información sobre licencias y otros aspectos de varias instancias de Salesforce en la empresa donde trabajo.

Al principio, accedía a cada una de ellas para obtener la info refrescando informes, consultando pantallas, etc., pero pronto me pareció repetitivo y tedioso. 
Analizando la API de Salesforce, me pareció que era posible acceder a mucha de la información que necesitaba.

Después de analizar la alternativa, confirmé que ejecutando _queries_ a los objetos adecuados de Salesforce es posible obtener información desde cualquier aplicación que pueda utilizar la API.

Propuse construir esa aplicación en mi empresa, pero desafortunadamente no fue aprobado, con lo que me la construí en mi tiempo libre. Es una aplicación Java sencilla para obtener los datos de Salesforce, y el framework Bootstrap para mostrar los resultados en una interfaz de usuario, que no dependa de nula habilidad como diseñador.

Si tienes esta misma necesidad, espero que te ayude o te de ideas.

## Acceder a la información de Licencias usadas ##
Toda la información que muestra la aplicación se obtienen mediante la ejecución de Queries sencillas. 

Como excepción y cómo indica esta página de Salesforce: [https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_userlicense.htm](UserLicense) varios de los campos necesarios para obtener la información de licencias usadas, requiere de abrir un ticket a soporte de Salesforce:
 
_This field is available through the API Access to User Licenses pilot program. For information on enabling this pilot program for your organization, contact Salesforce._

Yo lo hice sin problemas.

# Partes del proyecto #
___

Como comentaba el proyecto consta de 2 partes:

- La aplicación Java (en adelante Backend) que permite obtener la información que se desea de las instancias de Salesforce
- El Frontend que permite visualizar esos resultados en formato HTML5 via Bootstrap

Es decir, no he construido una MVC, ni nada por el estilo, sinó que:

1. La aplicación Java se conecta a Salesforce
2. Ejecuta las queries que le pasemos
3. Los resultado obtenidos se almacenan en ficheros JSON, ya que este formato es directamente consumible en Javascript
4. Con Bootstrap y jQuery se muestran los resultados, con los objetos de UI que ofrece el theme que he seleccionado

# Qué aspecto tiene #
___

El theme usado es **Gentelella** , muy conocido y accesible libremente en este Repo: [https://github.com/puikinsh/gentelella]
**Nota**: si no te gusta el theme y/o _Bootstrap_, cada uno puede crear su propio frontend sabiendo que los datos han sido generados en ficheros _*.json_.

## Página inicial ##

En la página inicial muestro los datos acumulados de los diferentes entornos. Esta es una página que consume los datos de las diferentes instancias para mostrar datos acumulados (decide si te parece interesante o elíminala).

Es decir, para mi es fundamental, mostrar la información de licenciamiento acumulado y por proyecto y detectar desviaciones.

Lo mismo puede hacerse para Límites, almacenamiento, llamadas, control de calidad, usuarios, etc., etc.

![alt text](./frontend/images/paginaresumen.png "Página Resumen")
 
Esta página contiene los elementos básicos de navegación:barra lateral de acceso, menú/submenús desplegables y contenido de la página

## Página de detalle por Instancia ##
En la página de detalle, se muestran los datos obtenidos de cada instancia de Salesforce:

1. Info básica de la ORG con el número de clases Apex y Test incluidas
2. Contacto principal y número de usuarios que aún se están conectado via TLS 1.0 (para evitar problemas cuando quede desactivado a mediados de 2017)
3. Información de licenciamiento
4. Información de almacenamiento
5. Información de acceso de usuarios detallada
6. Información de consumo sobre Límites

 ![alt text](./frontend/images/paginadetalle.png "Imagen Página Resumen")

Evidentemente esto es como me lo montado para mi, pero con concomientos básicos de _HTML5_ y _jQuery_ se puede mostrar lo que cada uno necesite.

# Como usarlo en mi empresa/cliente #
___

Supongamos que quieres probarlo, ¿qué tengo que hacer? Primero de todo clona el repo.

En el repo ya existen ficheros con datos, para que puedas acceder directamente. 

Por tanto si abres directamente fichero _/frontend/resumengeneral.html_ ya puedes navegar por la aplicación.


## Ejecución con tu ORGs ##

Lo más interesante es poderlo ver con los datos de las ORGs que te interesan. Suponiendo que has clonado el Repo en una máquina, con JDK instalado y con acceso de escritura en el directorio, sigue lo indicado a continuación.

### Creación del fichero de conexiones ###
1. En tu Sandbox crea una Connected App
2. A partir del fichero _conexiones-template.xml_ introduce los datos correspondientes y lo guardas con el nombre que quieras.
    1. Si mantienes la nomenclatura del tag `<descripcioorg>`con el valor _s1, s2, s3_ no tendrás que modificar nada
3. Desde la raiz del proyecto, ejecuta el comando: `java -jar /target/CMSFMultiOrg-1-jar-with-dependencies.jar nombrefichero_conexiones_del_paso_anterior`
4. La ejecución genera:
    1. Un directorio _Data_ con los resultados obtenidos
    2. Un fichero _all.log_ con el log de ejecución (también se muestra en pantalla) donde ver como ha transcurrido la ejecución
5. Una vez ejecutado correctamente, abre en tu navegador el fichero _/frontend/resumengeneral.html_ 

El fichero de conexiones puede contener tantas quieras como desees, y si utilizas hasta 3 descriptores _s1, s2, y _s3 no tendrás que modificar ningún aspecto en los ficheros del frontend.


 ![alt text](./frontend/images/ejemploficheroconexionex.png "Ejemplo Ficheros Conexiones")


Es importante el tag `<descripcionorg>` ya que los ficheros resultados se nombran con el valor del tag, y los ficheros HTML del frontend requieren leer esos ficheros.


## Mostrar los resultados ##
El tag `<descripcionorg>` se utiliza para nombrar los ficheros de datos. Si utilizas como convención, _s1, s2 y s3_, no tienes más que abrir el fichero _/frontend/resumengeneral.html_ para ver los resultados, en caso contrario, tendrás que modificar unas lineas de los ficheros del frontend, como se explica a continuación.

Si quieres crear más de 3 páginas o utilizar un _`<descripcionorg>`_ diferente a _s1, s2, s3_, solo tienes que:

1. En el directorio /frontend duplica el fichero _s1.html_, por ejemplo nómbralo s4 (debe coincidir con el valor que les hayas puesto en el tag `<descripcionorg>`)
2. Edita el fichero para hacer una modificación: al final del fichero modifica la parte indicada como _<!--Lectura de Ficheros de Datos-->_
3. Sustituye el sufijo s1 por el nombre que proporcionaste en el tag _`<descripcionorg>`_ en todas las lineas que cargan los ficheros `latest-*`

Ejemplo: renombramos los ficheros sustituyendo de s1 a s4 (en la imagen se muestra antes y después).

**Antes y Después**

 ![alt text](./frontend/images/DeS1aS4Antes.png "Fragmento original")

 ![alt text](./frontend/images/DeS1aS4Despues.png "Fragmento cambiado para la nueva página")
 
Para que la nueva página sea accesible desde cualquier otra página añádela en el menú lateral.

Ejemplo: abre la página _/frontend/resumengeneral.html_ y modifica la sección localizable en _&lt;!—sidebar menú --&gt;_

Tienes que repetirlo en todas las páginas desde donde quieras tener acceso. Son páginas completamente independientes que muestran la información que se quiera en cada página. Los desarrolladores que quieran pueden usar sistemas de plantillado, etc. (que queda fuera del alcance de lo que yo busco y necesito).

Ejemplo: añado acceso a la nueva página modificando el menú lateral
![alt text](./frontend/images/menumodificado.png "Fragmento original")

# Comentarios y Sugerencias #
___

<esteve.graells+bb@gmail.com>

# Licencia #
___

Este software está disponible bajo licencia MIT.



