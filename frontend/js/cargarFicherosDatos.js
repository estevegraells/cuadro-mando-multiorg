function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}

function loadJS(file) {
    // DOM: Create the script element
    var jsElm = document.createElement("script");
    // set the type attribute
    jsElm.type = "application/javascript";
    // make the script element load file
    jsElm.src = file;
    // finally insert the element to the body element in order to load the script
    document.body.appendChild(jsElm);
}


//Carga de los ficheros de la ORG seleccionada
loadJS("latest-OrgInfo-" 		+ GetURLParameter("org") + ".js");
loadJS("latest-LicenciasInfo-" 		+ GetURLParameter("org") + ".js");
loadJS("latest-UsuariosTLS107dias-" 	+ GetURLParameter("org") + ".js");
loadJS("latest-UsuariosTLS10ayer-"  	+ GetURLParameter("org") + ".js");
loadJS("latest-UsuariosInactivos30-" 	+ GetURLParameter("org") + ".js");
loadJS("latest-UsuariosInactivos60-" 	+ GetURLParameter("org") + ".js");
loadJS("latest-UsuariosInactivos90-" 	+ GetURLParameter("org") + ".js");
loadJS("latest-UsuariosActivos30-" 	+ GetURLParameter("org") + ".js");
loadJS("latest-NuncaLogin-" 		+ GetURLParameter("org") + ".js");
loadJS("latest-Limits-" 		+ GetURLParameter("org") + ".js");
loadJS("latest-Control-" 		+ GetURLParameter("org") + ".js");
loadJS("latest-JobArquitectura-" 	+ GetURLParameter("org") + ".js");
loadJS("latest-ApexCodeTotal-" 		+ GetURLParameter("org") + ".js");
loadJS("latest-ApexCodeTest-" 		+ GetURLParameter("org") + ".js");

