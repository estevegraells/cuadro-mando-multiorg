//Este es un ejemplo de como mediante jQuery capturamos los datos de los diferentes ficheros de datos generados
//y montamos una página con datos acumulados de nuestros sandboxes/ORGs



$(function() {

    /** Calculo suma de todas las licencias asignadas**/
    var l_sales_asig = 0;
    $(".l_sales_asig").each(function() {

        var value = $(this).text();
        if(!isNaN(value) && value.length != 0) {
            l_sales_asig += parseFloat(value);
        }
    });
    l_sales_asig -= $(".l_sop_asig").text();
    $("#lic_sales_asig_total").html(l_sales_asig);

    
    /** Cálculo suma de todas las licencias activadas**/
    var l_sales_acti = 0;
    // iterate through each td based on class and add the values
    $(".l_sales_acti").each(function() {

        var value = $(this).text();
        if(!isNaN(value) && value.length != 0) {
            l_sales_acti += parseFloat(value);
        }
    });
    l_sales_acti -= $(".l_sop_acti").text();
	$("#lic_sales_acti_total").html(l_sales_acti);
	
	
	/** Calculo suma de todas las licencias disponibles**/
    var l_sales_dife = 0;
    // iterate through each td based on class and add the values
    $(".l_sales_dife").each(function() {

        var value = $(this).text();
        if(!isNaN(value) && value.length != 0) {
            l_sales_dife += parseFloat(value);
        }
    });
	l_sales_dife -= $(".l_sop_dife").text();
    $("#lic_sales_dife_total").html(l_sales_dife);
	

    /** Calculo suma de todas las licencias asignadas **/
    var l_partner_asig = 0;
    $(".l_partner_asig").each(function() {
        var value = $(this).text();
        if(!isNaN(value) && value.length != 0) {
            l_partner_asig += parseFloat(value);
        }
    });
    $("#lic_partner_asig_total").html(l_partner_asig);

    /** Calculo suma de todas las licencias disponibles **/
    var l_partner_acti = 0;
    $(".l_partner_acti").each(function() {
        var value = $(this).text();
        if(!isNaN(value) && value.length != 0) {
            l_partner_acti += parseFloat(value);
        }
    });
    $("#lic_partner_acti_total").html(l_partner_acti);
	
	/** Calculo suma de todas las licencias disponibles **/
    var l_partner_dife = 0;
    $(".l_partner_dife").each(function() {
        var value = $(this).text();
        if(!isNaN(value) && value.length != 0) {
            l_partner_dife += parseFloat(value);			
        }
		
    });
    $("#lic_partner_dife_total").html(l_partner_dife);
	

    //Actualizar los valores de los gráficos
	//ADAPTAR: incluir el numero total de licencias Sales&service que tengas contratado en tu empresa
	//ADAPTAR: incluir el numero total de licencias Partner que tengas contratado en tu empresa
    barChartLicSales.data.datasets[0].data = [1000, l_sales_asig, l_sales_acti, l_sales_dife];
    barChartLicSales.update();
    barChartLicPartner.data.datasets[0].data = [5000, l_partner_asig, l_partner_acti, l_partner_dife];
    barChartLicPartner.update();

});


// ADADTAR: para mostrar los datos correctamente modifica el valor de licencias totales que teneis adquiridas en vuestra empresa/cliente
/**** Diagramas de barras ***/
Chart.defaults.global.legend = {
    enabled: true
};

function drawBarValues()
{
  // render the value of the chart above the bar
  var ctx = this.chart.ctx;
  ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
  ctx.fillStyle = this.chart.config.options.defaultFontColor;
  ctx.textAlign = 'center';
  ctx.textBaseline = 'bottom';
  this.data.datasets.forEach(function (dataset) {
    for (var i = 0; i < dataset.data.length; i++) {
      if(dataset.hidden === true && dataset._meta[Object.keys(dataset._meta)[0]].hidden !== false){ continue; }
      var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
      if(dataset.data[i] !== null){
        ctx.fillText(dataset.data[i], model.x - 1, model.y - 5);
      }
    }
  });
}


var ctx = document.getElementById("barChartLicSales");
var barChartLicSales = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ["Compradas", "Asignadas", "Activadas", "Disponibles"],
      datasets: [{
        label: 'Licencias',
        backgroundColor: [
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(0, 255, 0, 0.2)',
                        'rgba(128,0,255, 0.2)',
						'rgba(255, 99, 132, 0.2)'
        ],
        borderColor: [
            'rgba(75, 192, 192, 1)',
            'rgba(0, 255, 0, 1)',
            'rgba(128,0,255,1)',
            'rgba(255,99,132,1)'
        ],
        borderWidth: 1,
        data: [1000, 0, 0] //ADAPTAR: incluir el numero total de licencias Sales&service que tengas contratado en tu empresa
      }]
    },
    options: {
      animation: {
            onProgress: drawBarValues,
            onComplete: drawBarValues
          },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
});

// Bar chart
var ctx = document.getElementById("barChartLicPartner");
var barChartLicPartner = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ["Compradas", "Asignadas", "Activadas", "Disponibles"],
      datasets: [{
        label: 'Licencias',
        backgroundColor: [
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(0, 255, 0, 0.2)',
                        'rgba(128,0,255, 0.2)',
                        'rgba(255, 99, 132, 0.2)'
        ],
        borderColor: [
            'rgba(75, 192, 192, 1)',
            'rgba(0, 255, 0, 1)',
            'rgba(128,0,255,1)',
            'rgba(255,99,132,1)'
        ],
        borderWidth: 1,
        data: [5000, 0, 0] //ADAPTAR: incluir el numero total de licencias Partner que tengas contratado en tu empresa
      }]
    },
    options: {
      animation: {
            onProgress: drawBarValues,
            onComplete: drawBarValues
          },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
});


	