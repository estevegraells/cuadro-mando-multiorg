$(function() {
    
   
   	var strORG = "ORG:  ";
	
	if (OrgInfo.records.length>0){
		$("#titulo").html(strORG.concat(OrgInfo.records["0"].Name));
	}

	var LicenciasSalesTotales 	= 0;
	var LicenciasSalesUsadas 	= 0;
	var LicenciasPartnerTotales 	= 0;
	var LicenciasPartnerUsadas 	= 0;
	var LicenciasForceTotales 	= 0;
	var LicenciasForceUsadas 	= 0;	
		                          
	//Obtener la info de las licencias procedentes de los ficheros generados
	if (typeof LicenciasInfo != "undefined") { 
		for (var i = 0; i < LicenciasInfo.records.length; i++) {
    		
    		if (LicenciasInfo.records[i].Name == "Salesforce"){
			LicenciasSalesTotales 	= LicenciasInfo.records[i].TotalLicenses;
			LicenciasSalesUsadas 	= LicenciasInfo.records[i].UsedLicenses;		
		}else if (LicenciasInfo.records[i].Name == "Partner Community"){
			LicenciasPartnerTotales	= LicenciasInfo.records[i].TotalLicenses;
			LicenciasPartnerUsadas 	= LicenciasInfo.records[i].UsedLicenses;		
		}else if (LicenciasInfo.records[i].Name == "Force.com"){
        			LicenciasForceTotales 	= LicenciasInfo.records[i].TotalLicenses;
        			LicenciasForceUsadas 	= LicenciasInfo.records[i].UsedLicenses;		
        		} 
        	}
	}
		
	$("#licenciasSalesAsignadas").html(LicenciasSalesTotales);
	$("#licenciasSalesUsadas").html(LicenciasSalesUsadas);
	$("#licenciasSalesDisponibles").html(LicenciasSalesTotales - LicenciasSalesUsadas);
	if ( (LicenciasSalesTotales <0 ) && (LicenciasSalesTotales - LicenciasSalesUsadas) < 5 ) {	
		$("#licenciasSalesDisponibles").css('background-color', 'red');
	}
	
	$("#licenciasPartnerAsignadas").html(LicenciasPartnerTotales);
	$("#licenciasPartnerUsadas").html(LicenciasPartnerUsadas);
	$("#licenciasPartnerDisponibles").html(LicenciasPartnerTotales - LicenciasPartnerUsadas);
	if ( (LicenciasPartnerTotales > 0) && (LicenciasPartnerTotales - LicenciasPartnerUsadas) < 5 ) {	
		$("#licenciasPartnerDisponibles").css('background-color', 'red');
	}
	
	$("#licenciasForceAsignadas").html(LicenciasForceTotales);
	$("#licenciasForceUsadas").html(LicenciasForceUsadas);
	$("#licenciasForceDisponibles").html(LicenciasForceTotales - LicenciasForceUsadas);
	
	$('#NumeroUsuariosTLS10').html(UsuariosTLS10ayer.records[0].expr0 + "/" + UsuariosTLS107dias.records[0].expr0);
	
	if ( (UsuariosTLS107dias.records[0].expr0 > 0) || (UsuariosTLS10ayer.records[0].expr0 > 0) ) {
	    $('#NumeroUsuariosTLS10').css('color', 'red');
	}
	
	$("#nuncalogin").html(NuncaLogin.records[0].expr0);

	$("#usactivos30dias").html(UsuariosActivos30.records[0].expr0);
	$("#usinactivos30dias").html(UsuariosInactivos30.records[0].expr0);

	$("#usinactivos60dias").html(UsuariosInactivos60.records[0].expr0);

	$("#usinactivos90dias").html(UsuariosInactivos90.records[0].expr0);

	$("#TotalDataStorage").html("Total: " + Math.floor(limits.DataStorageMB.Max / 1024) + " GB");
	$("#LibreDataStorage").html("Disponible: " + Math.floor(limits.DataStorageMB.Remaining / 1024 )+ " GB");

	var PorcentajeDataValor= Math.floor (100 * ( (limits.DataStorageMB.Max - limits.DataStorageMB.Remaining) / limits.DataStorageMB.Max));
	$("#PorcentajeData").html(Math.floor (PorcentajeDataValor) + "% ocupado");
	$('#PorcentajeDataDonut').attr('data-percent', PorcentajeDataValor);
	$("#file_storage_ocupado").html(limits.DataStorageMB.Max);
	$("#file_storage_disponible").html(limits.DataStorageMB.Remaining);

	$("#TotalFileStorage").html("Total: " + Math.floor(limits.FileStorageMB.Max / 1024) + " GB");
	$("#LibreFileStorage").html("Disponible: " + Math.floor (limits.FileStorageMB.Remaining / 1024 ) + " GB");
	$("#data_storage_ocupado").html(limits.FileStorageMB.Max);
	$("#data_storage_disponible").html(limits.FileStorageMB.Remaining);

	var PorcentajeFileValor = Math.floor (100 * ( (limits.FileStorageMB.Max - limits.FileStorageMB.Remaining) / limits.FileStorageMB.Max));
	$("#PorcentajeFile").html(Math.floor (PorcentajeFileValor) + "% ocupado");
	$('#PorcentajeFileDonut').attr('data-percent', PorcentajeFileValor);
	
	//Limites
	$("#DailyApiRequestsMax").html(limits.DailyApiRequests.Max);
	$("#DailyApiRequestsRemaining").html(limits.DailyApiRequests.Remaining);
	
	$("#DailyAsyncApexExecutionsMax").html(limits.DailyAsyncApexExecutions.Max);
	$("#DailyAsyncApexExecutionsRemaining").html(limits.DailyAsyncApexExecutions.Remaining);
	
	$("#DailyBulkApiRequestsMax").html(limits.DailyBulkApiRequests.Max);
	$("#DailyBulkApiRequestsRemaining").html(limits.DailyBulkApiRequests.Remaining);
	
	$("#DailyWorkflowEmailsMax").html(limits.DailyWorkflowEmails.Max);
	$("#DailyWorkflowEmailsRemaining").html(limits.DailyWorkflowEmails.Remaining);
	
	$("#HourlyTimeBasedWorkflowMax").html(limits.HourlyTimeBasedWorkflow.Max);
	$("#HourlyTimeBasedWorkflowRemaining").html(limits.HourlyTimeBasedWorkflow.Remaining);
	
	$("#MassEmailMax").html(limits.MassEmail.Max);
	$("#MassEmailRemaining").html(limits.MassEmail.Remaining);

	$("#SingleEmailMax").html(limits.SingleEmail.Max);
	$("#SingleEmailRemaining").html(limits.SingleEmail.Remaining);
	
	$('#apex').html(ApexCodeTotal.records[0].expr0 + " - " + ApexCodeTest.records[0].expr0);

	 $("#nombreORG").html(OrgInfo.records[0].Name);
	 $("#contactoORG").html(OrgInfo.records[0].PrimaryContact);
	if (OrgInfo.isSandbox == "true" > 0){
	    $("#idORG").html(OrgInfo.records[0].Id + " - Sandbox no Productivo");
	}else{
	     $("#idORG").html(OrgInfo.records[0].Id + " - Productivo / Full Sandbox");
	}

	$("#myDomainORG").html(control.mydomain);

	//Particularidades por ORG: mediante este código es senzillo particularizar datos de cada Página
	//El name es el nombre de la ORG que llega de la llamada API
	/* if ( OrgInfo.records[0].Name == 'Loquesea'){
		$("#CompradoDataStorage").html( "Texto alertando o informando de algo");
		$("#TodoLicenciasDataStorage").hide();
		$("#CompradoFileStorage").hide();
	} else if ( OrgInfo.records[0].Name  == 'Otra'){
		$("#CompradoDataStorage").html( "otro texto, el que sea, pero que será diferente que el anterior");
		$("#TodoLicenciasDataStorage").hide();
		$("#CompradoFileStorage").hide();
	}else{
		$("#CompradoDataStorage").hide();
		$("#CompradoFileStorage").hide();
		$("#TodoLicenciasFileStorage").show();
		$("#TodoLicenciasDataStorage").show();
	}*/
	 
	 //Si alguno de los % de ocupación supera el 100% marcamos el valor rojo.
	 var colorDonete ="#26B99A";
	 if (PorcentajeDataValor>100 || PorcentajeFileValor > 100){
	     colorDonete = '#d10c33';
	 }
	
    	 $('.chart').easyPieChart({
     	  easing: 'easeOutElastic',
     	  delay: 6000,
     	  barColor: colorDonete,
     	  trackColor: 'lightgrey',
     	  scaleColor: false,
     	  lineWidth: 20,
     	  trackWidth: 16,
     	  lineCap: 'butt',
     	  onStep: function(from, to, percent) {
     		$(this.el).find('.percent').text(Math.round(percent));
     	  }
     	 });
	 

  });
  






  
 