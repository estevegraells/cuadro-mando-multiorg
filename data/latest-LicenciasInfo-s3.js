var LicenciasInfo = {
  "totalSize" : 9,
  "done" : true,
  "records" : [ {
    "attributes" : {
      "type" : "UserLicense",
      "url" : "/services/data/v39.0/sobjects/UserLicense/100240000006rgwAAA"
    },
    "Name" : "Salesforce",
    "Status" : "Active",
    "TotalLicenses" : 23,
    "UsedLicenses" : 21
  }, {
    "attributes" : {
      "type" : "UserLicense",
      "url" : "/services/data/v39.0/sobjects/UserLicense/10024000000LDXiAAO"
    },
    "Name" : "Chatter Free",
    "Status" : "Active",
    "TotalLicenses" : 5000,
    "UsedLicenses" : 1
  }, {
    "attributes" : {
      "type" : "UserLicense",
      "url" : "/services/data/v39.0/sobjects/UserLicense/10024000000LDXjAAO"
    },
    "Name" : "Chatter External",
    "Status" : "Active",
    "TotalLicenses" : 500,
    "UsedLicenses" : 1
  }, {
    "attributes" : {
      "type" : "UserLicense",
      "url" : "/services/data/v39.0/sobjects/UserLicense/10024000000LDXnAAO"
    },
    "Name" : "Partner Community",
    "Status" : "Active",
    "TotalLicenses" : 1200,
    "UsedLicenses" : 345
  }, {
    "attributes" : {
      "type" : "UserLicense",
      "url" : "/services/data/v39.0/sobjects/UserLicense/10024000000LDXoAAO"
    },
    "Name" : "Partner Community Login",
    "Status" : "Disabled",
    "TotalLicenses" : 0,
    "UsedLicenses" : 0
  }, {
    "attributes" : {
      "type" : "UserLicense",
      "url" : "/services/data/v39.0/sobjects/UserLicense/10024000000LDYoAAO"
    },
    "Name" : "Force.com - App Subscription",
    "Status" : "Disabled",
    "TotalLicenses" : 0,
    "UsedLicenses" : 0
  }, {
    "attributes" : {
      "type" : "UserLicense",
      "url" : "/services/data/v39.0/sobjects/UserLicense/10024000000LFoVAAW"
    },
    "Name" : "Guest",
    "Status" : "Active",
    "TotalLicenses" : 1,
    "UsedLicenses" : 1
  }, {
    "attributes" : {
      "type" : "UserLicense",
      "url" : "/services/data/v39.0/sobjects/UserLicense/10024000000LIi6AAG"
    },
    "Name" : "Guest User License",
    "Status" : "Active",
    "TotalLicenses" : 25,
    "UsedLicenses" : 0
  }, {
    "attributes" : {
      "type" : "UserLicense",
      "url" : "/services/data/v39.0/sobjects/UserLicense/10024000000MDTLAA4"
    },
    "Name" : "Premier Support",
    "Status" : "Disabled",
    "TotalLicenses" : 0,
    "UsedLicenses" : 0
  } ]
}
