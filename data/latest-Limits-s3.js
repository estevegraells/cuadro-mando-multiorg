var limits = {
  "ConcurrentAsyncGetReportInstances" : {
    "Max" : 200,
    "Remaining" : 200
  },
  "ConcurrentSyncReportRuns" : {
    "Max" : 20,
    "Remaining" : 20
  },
  "DailyApiRequests" : {
    "Max" : 1570000,
    "Remaining" : 1569882,
    "Ant Migration Tool" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "ArquitecturaInfo" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Chatter Desktop" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Chatter Mobile for BlackBerry" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Dataloader Bulk" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Dataloader Partner" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Force.com IDE" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Núcleo de entorno" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Chatter" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Files" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Mobile Dashboards" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Touch" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce for Outlook" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce1 for Android" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce1 for iOS" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "SalesforceA" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Workbench" : {
      "Max" : 0,
      "Remaining" : 0
    }
  },
  "DailyAsyncApexExecutions" : {
    "Max" : 250000,
    "Remaining" : 250000
  },
  "DailyBulkApiRequests" : {
    "Max" : 10000,
    "Remaining" : 10000,
    "Ant Migration Tool" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "ArquitecturaInfo" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Chatter Desktop" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Chatter Mobile for BlackBerry" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Dataloader Bulk" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Dataloader Partner" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Force.com IDE" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Núcleo de entorno" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Chatter" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Files" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Mobile Dashboards" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Touch" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce for Outlook" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce1 for Android" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce1 for iOS" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "SalesforceA" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Workbench" : {
      "Max" : 0,
      "Remaining" : 0
    }
  },
  "DailyDurableGenericStreamingApiEvents" : {
    "Max" : 1000000,
    "Remaining" : 1000000
  },
  "DailyDurableStreamingApiEvents" : {
    "Max" : 1000000,
    "Remaining" : 1000000
  },
  "DailyGenericStreamingApiEvents" : {
    "Max" : 10000,
    "Remaining" : 10000,
    "Ant Migration Tool" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "ArquitecturaInfo" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Chatter Desktop" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Chatter Mobile for BlackBerry" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Dataloader Bulk" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Dataloader Partner" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Force.com IDE" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Núcleo de entorno" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Chatter" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Files" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Mobile Dashboards" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Touch" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce for Outlook" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce1 for Android" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce1 for iOS" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "SalesforceA" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Workbench" : {
      "Max" : 0,
      "Remaining" : 0
    }
  },
  "DailyStreamingApiEvents" : {
    "Max" : 1000000,
    "Remaining" : 1000000,
    "Ant Migration Tool" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "ArquitecturaInfo" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Chatter Desktop" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Chatter Mobile for BlackBerry" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Dataloader Bulk" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Dataloader Partner" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Force.com IDE" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Núcleo de entorno" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Chatter" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Files" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Mobile Dashboards" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce Touch" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce for Outlook" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce1 for Android" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Salesforce1 for iOS" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "SalesforceA" : {
      "Max" : 0,
      "Remaining" : 0
    },
    "Workbench" : {
      "Max" : 0,
      "Remaining" : 0
    }
  },
  "DailyWorkflowEmails" : {
    "Max" : 311000,
    "Remaining" : 311000
  },
  "DataStorageMB" : {
    "Max" : 39132,
    "Remaining" : 18132
  },
  "DurableStreamingApiConcurrentClients" : {
    "Max" : 20,
    "Remaining" : 20
  },
  "FileStorageMB" : {
    "Max" : 711215,
    "Remaining" : 111204
  },
  "HourlyAsyncReportRuns" : {
    "Max" : 1200,
    "Remaining" : 1200
  },
  "HourlyDashboardRefreshes" : {
    "Max" : 200,
    "Remaining" : 200
  },
  "HourlyDashboardResults" : {
    "Max" : 5000,
    "Remaining" : 5000
  },
  "HourlyDashboardStatuses" : {
    "Max" : 999999999,
    "Remaining" : 999999999
  },
  "HourlyODataCallout" : {
    "Max" : 10000,
    "Remaining" : 10000
  },
  "HourlySyncReportRuns" : {
    "Max" : 500,
    "Remaining" : 500
  },
  "HourlyTimeBasedWorkflow" : {
    "Max" : 50,
    "Remaining" : 50
  },
  "MassEmail" : {
    "Max" : 5000,
    "Remaining" : 5000
  },
  "SingleEmail" : {
    "Max" : 5000,
    "Remaining" : 5000
  },
  "StreamingApiConcurrentClients" : {
    "Max" : 2000,
    "Remaining" : 2000
  }
}
