package com.egraells;

import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.NodeList;

class cargarConexiones {
    
    static final DocumentBuilderFactory dbFactory = null;
    static final DocumentBuilder docBuilder = null;
    static final File fichConexiones = null;
    static NodeList connectionList = null;
    
    public NodeList getConnectionsList() { return connectionList;}
    
    public cargarConexiones(String Fichero){
        
        Document xmlEntrada = null;
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
                        
            //Configuracion fichero XML entrada
            File fConexiones = new File(Fichero);
            xmlEntrada = docBuilder.parse(fConexiones);
            xmlEntrada.getDocumentElement().normalize();// optional, but recommended read this -http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work

            //Configuracion fichero XML salida de resultados
            Document xmlSalida = docBuilder.newDocument();
            
            //creacion nodo raiz
            Element rootElement = xmlSalida.createElement("licenciasorgs");
            xmlSalida.appendChild(rootElement);
        } catch (Exception e) {
        
            e.printStackTrace();
        }    
        
        //Lectura del fichero de conexiones
        connectionList = xmlEntrada.getElementsByTagName("conexion");
    }

}
