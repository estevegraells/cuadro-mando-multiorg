/**
* Obtener informacion ORGs de Salesforce.
* 	
* Descripcion: A partir de un fichero de conexiones de entrada se generan ficheros JSON con la info obtenida de todas las orgs.
* 
*  Para obtener la informacion de las ORG se accede mediante REST API.
*  En concreto para la obtenión de la información de licencias, se usa el objeto UserLicense 
*  https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_userlicense.htm
*  Varios de los campos usados en la query requieren la apertura de un ticket a Salesforce para que activen la capacidad de consultarlos:
*  This field is available through the API Access to User Licenses pilot program. For information on enabling this pilot program for your organization, contact Salesforce.
*  *  
*  
* Descarga de las librer�as de Salesforce: http://mvnrepository.com/artifact/com.force.api
*
* @author  Esteve Graells 
* @version 1.0
* @since   01-08-2016 
*/

package com.egraells;

import java.io.File;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicHeader;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


public class CMSFMultiOrg {

	private static String REST_ENDPOINT 				= "/services/data" ;
    private static String API_VERSION 					= "/v39.0" ;
    
	private static String loginAccessToken 				= "";
	private static String loginInstanceUrl 				= "";
	private static String descORG 						= "";
	private static HttpClient httpClient				= null;

	private static final String qOrgInfo				= "SELECT CreatedDate,Id,IsSandbox,Name, OrganizationType,PrimaryContact FROM Organization";
	private static final String qNuncaLogin 			= "SELECT count(id) FROM User WHERE LastLoginDate = NULL AND CreatedDate != LAST_N_DAYS:30 AND IsActive=TRUE";
	private static final String qTLS107dias		 		= "SELECT count(id) FROM LoginHistory WHERE TlsProtocol= 'TLS 1.0' AND LoginTime = LAST_N_DAYS:7 ";
	private static final String qTLS10ayer		 		= "SELECT count(id) FROM LoginHistory WHERE TlsProtocol= 'TLS 1.0' AND LoginTime = LAST_N_DAYS:1 ";
	private static final String qInactivos30 			= "SELECT count(id) FROM User WHERE LastLoginDate != LAST_N_DAYS:30 AND CreatedDate != LAST_N_DAYS:90 AND IsActive=TRUE";
	private static final String qInactivos60 			= "SELECT count(id) FROM User WHERE LastLoginDate != LAST_N_DAYS:60 AND CreatedDate != LAST_N_DAYS:90 AND IsActive=TRUE";
	private static final String qInactivos90 			= "SELECT count(id) FROM User WHERE LastLoginDate != LAST_N_DAYS:90 AND CreatedDate != LAST_N_DAYS:90 AND IsActive=TRUE";
	private static final String qActivos30				= "SELECT count(id) FROM User WHERE LastLoginDate = LAST_N_DAYS:30 AND IsActive=TRUE";
	private static final String qLicencias				= "SELECT Name, Status, TotalLicenses, UsedLicenses FROM UserLicense";//requiere activación de acceso al objeto UserLicense por parte de Salesforce
	private static final String qJobArquitectura		= "SELECT id FROM CronJobDetail WHERE Name='Storage Alarms Notification ARQ-GNF'";
	private static final String qApexCodeTotal			= "SELECT count(id) FROM apexclass";
	private static final String qApexCodeTest			= "SELECT count(id) FROM apexclass WHERE name like '%Test'";
	
	
	static final Logger logger = LogManager.getLogger(CMSFMultiOrg.class.getName());
	
	public static void main(String[] args) {
		
		logger.info("Inicio");
		
		//Obtención del path al fichero de conexiones por parametro
		if ((args != null) && (args.length == 1)){
			
			logger.info("El fichero de conexiones es: " + args[0] );
		
		}else{
			logger.error("El programa requiere un parámetro con el path completo al fichero de conexiones");
			System.exit(-1);
		}

		//Lectura del fichero de conexiones
		cargarConexiones readConnections = new cargarConexiones(args[0]);
		
		NodeList connectionsList = readConnections.getConnectionsList();
		try {
			
			//Para cada conexión 
			for (int temp = 0; temp < connectionsList.getLength(); temp++) {
				Node nNode = connectionsList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement    = (Element) nNode;
					descORG 			= eElement.getElementsByTagName("descripcionorg").item(0).getTextContent();
					String username 	= eElement.getElementsByTagName("username").item(0).getTextContent();
					String password 	= eElement.getElementsByTagName("password").item(0).getTextContent();
					String token 		= eElement.getElementsByTagName("token").item(0).getTextContent();
					String loginURL 	= eElement.getElementsByTagName("loginurl").item(0).getTextContent();
					String clientId 	= eElement.getElementsByTagName("clientid").item(0).getTextContent();
					String clientSecret	= eElement.getElementsByTagName("clientsecret").item(0).getTextContent();

					//Establecer conexion con la ORG
					obtenerConexionSF sfCon = new obtenerConexionSF(username, password, token, loginURL, clientId, clientSecret);
					
					loginAccessToken = sfCon.getloginAccessToken();
				    loginInstanceUrl = sfCon.getloginInstanceUrl();
				    logger.info("Login Correcto: " + loginInstanceUrl);
			

				    //Set up the HTTP objects needed to make the request.
			        httpClient = HttpClientBuilder.create().build();

			        //Creación del directorio de resultados si no existiera
			        File dataDir = new File("data");
			        
			        if (!dataDir.isDirectory()) Files.createDirectories(Paths.get("data"));
					logger.info("En el directorio Data se guardan los resultados");
					
					
			        //Obtener el consumo de Limites de la ORG
			        obtenerInfoLimites();
			        
			        //Para cada una de las queries, generaremos una variable principal del fichero JSON 
			        //Esta variable es usada en los ficheros Javascript del frontend para parsear y obtener los resultados
			        
			        realizarConsulta(qOrgInfo, 			"OrgInfo");
			        realizarConsulta(qTLS107dias,		"UsuariosTLS107dias");
			        realizarConsulta(qTLS10ayer,		"UsuariosTLS10ayer");
			        realizarConsulta(qInactivos30, 		"UsuariosInactivos30");
			        realizarConsulta(qInactivos60, 		"UsuariosInactivos60");
			        realizarConsulta(qInactivos90, 		"UsuariosInactivos90");
			        realizarConsulta(qActivos30, 		"UsuariosActivos30");
			        realizarConsulta(qNuncaLogin, 		"NuncaLogin");
			        realizarConsulta(qLicencias, 		"LicenciasInfo");//requiere activación de acceso al objeto UserLicense por parte de Salesforce
			        realizarConsulta(qApexCodeTest, 	"ApexCodeTest");
			        realizarConsulta(qApexCodeTotal, 	"ApexCodeTotal");
			        realizarConsulta(qJobArquitectura, 	"JobArquitectura");
			        
			        //Obtener el dominio completo de la ORG
			        obtenerDominioCompleto();
			        
					logger.info("El proceso ha finalizado");
				}
			}
		} catch (Exception e) {e.printStackTrace();}
	}


	//Obtener los datos sobre limites de la ORG
	private static void obtenerInfoLimites() {
	
		String baseUri = loginInstanceUrl + REST_ENDPOINT + API_VERSION ;
		Header oauthHeader = new BasicHeader("Authorization", "OAuth " + loginAccessToken) ;
		Header prettyPrintHeader = new BasicHeader("X-PrettyPrint", "1");
		
		try {
			String uri = baseUri + "/limits";
	        
	        HttpGet httpGet = new HttpGet(uri);
	        httpGet.addHeader(oauthHeader);
	        httpGet.addHeader(prettyPrintHeader);
		
	        //Enviamos la peticion 
			HttpResponse response = httpClient.execute(httpGet);
			
			// Gestión del resultado
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
			    
				//Creamos el contenido en formato JSON
				String response_string = "var limits" + " = "  + EntityUtils.toString(response.getEntity());

			    try {
			    	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
					LocalDateTime now = LocalDateTime.now();
					
					//Guardarmos el fichero resultado con la nomenclatura específica que espera el frontend utiliza
			        PrintWriter out1 = new PrintWriter( ".//data//" + dtf.format(now) + "-" +  "Limits"  + "-" + descORG + ".js" );
		            out1.println( response_string ); 
		            out1.close();
		            
		            //El mismo fichero lo guardamos para historico. De momento no hacemos nada con él
		            PrintWriter out2 = new PrintWriter( ".//data//latest-" +  "Limits" + "-" + descORG + ".js" );
		            out2.println( response_string ); //save to file
		            out2.close();
		            
			    } catch (Exception e) {
			        e.printStackTrace();
			    }
			    
			} else {
				
				logger.info("Se ha producido un error en la consulta, código:  " + statusCode + " - " + response.getStatusLine().getStatusCode());
			    System.exit(-1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	//Ejecución de la query que se desea y generación de un fichero JSON con los resultados 
	private static void realizarConsulta(String Query, String tipoDato) {
		
		logger.info("Query: " + Query + " para ORG: " + descORG );
		
		String baseUri = loginInstanceUrl + REST_ENDPOINT + API_VERSION ;
		Header oauthHeader = new BasicHeader("Authorization", "OAuth " + loginAccessToken) ;
		Header prettyPrintHeader = new BasicHeader("X-PrettyPrint", "1");
		
		try {
			
			String queryEncoded = URLEncoder.encode(Query, "UTF-8");
			String uri = baseUri + "/query?q=" + queryEncoded;
	        			
	        HttpGet httpGet = new HttpGet(uri);
	        httpGet.addHeader(oauthHeader);
	        httpGet.addHeader(prettyPrintHeader);
		
			HttpResponse response = httpClient.execute(httpGet);
			
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				
				//Creamos el contenido en formato JSON
			    String response_string = "var " + tipoDato + " = "  + EntityUtils.toString(response.getEntity());
			    try {
			    	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
					LocalDateTime now = LocalDateTime.now();
					
					//Guardarmos el fichero resultado con la nomenclatura específica que espera el frontend mostrará 
		            PrintWriter ficheroResultado = new PrintWriter( ".//data//latest-" +  tipoDato + "-" + descORG + ".js" );
		            ficheroResultado.println( response_string ); //save to file
		            ficheroResultado.close();
					
		         	//El mismo fichero lo guardamos para historico. De momento no hacemos nada con él
			        PrintWriter ficheroHistorico = new PrintWriter( ".//data//" +  dtf.format(now) + "-" +  tipoDato  + "-" + descORG + ".js" );
		            ficheroHistorico.println( response_string ); //save to file
		            ficheroHistorico.close();
		            
			    } catch (Exception e) {
			        e.printStackTrace();
			    }
			} else {
			    logger.error("Se ha producido un error en la consulta, código:  " + statusCode + " - " + response.getStatusLine().getStatusCode());
			    System.exit(-1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	//Obtener el dominio completo de la ORG y añadimos la fecha de ejecución como control
	private static void obtenerDominioCompleto() {
		
		try {
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-YYYY");
				LocalDateTime now = LocalDateTime.now();
		    	
		        PrintWriter out1 = new PrintWriter( ".//data//latest-Control-" + descORG + ".js" );
	            out1.println ( "var control = {" );

	            out1.println ( " \"fecha\" : \"" +  dtf.format(now) + "\","); 
	            out1.println ( " \"mydomain\" : \"" + loginInstanceUrl + "\""); 

	            out1.println( "} //FINALIZACION CORRECTA"); 
	            out1.close();
	            
	    } catch (Exception e) {
	        e.printStackTrace();
	    }

	}
	
}
